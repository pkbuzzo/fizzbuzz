function fizzBuzz(n) {
    function fizzBuzzRecursive(n) {
      var array = [];
      if (n === 1) {
        return '1';
      } else {
        if (n % 3 === 0 && n % 2 === 0) {
          array.push(n + 'FizzBuzz');
        } else if (n % 3 === 0) {
          array.push(n + 'Buzz');
        } else if (n % 2 === 0) {
          array.push(n + 'Fizz');
        } else {
          array.push(n);
        }
        return array.concat(fizzBuzzRecursive(n - 1));
      }
    }
    
    return fizzBuzzRecursive(n).reverse();
  };
  
  document.write(fizzBuzz(100))